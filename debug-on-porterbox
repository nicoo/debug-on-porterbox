#!/usr/bin/python3

# Copyright (C) 2018 Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import paramiko
import socket
import argparse
import logging
import time
import shlex
import subprocess
import os
import re
import sys
import codecs
import ldap3
import git

log = logging.getLogger()


class Fail(RuntimeError):
    pass


def is_hostname(host_or_arch):
    log.debug("Resolving %s.debian.org to check if it is already a hostname", host_or_arch)
    try:
        socket.getaddrinfo(host_or_arch + ".debian.org", 22, proto=socket.IPPROTO_TCP)
    except socket.gaierror as e:
        if e.errno == socket.EAI_NODATA:
            return False
        else:
            raise
    return True


def find_porterboxes(arch):
    log.info("Getting list of porterboxes from LDAP")
    conn = ldap3.Connection("db.debian.org", auto_bind=True)
    conn.search('dc=debian,dc=org', '(&(objectClass=debianServer)(purpose=porterbox))', attributes=["*"])
    hosts = []
    for entry in conn.entries:
        arches = str(entry.description).split(None, 1)[0].split("/")
        if arch in arches:
            hosts.append(str(entry.hostname))
    return hosts


def find_host(arch):
    log.debug("%s does not look like a hostname: looking up porterboxes on LDAP", arch)
    hosts = find_porterboxes(arch)
    if len(hosts) > 1:
        print("{} hosts found for arch {}: {}".format(len(hosts), arch, ", ".join(hosts)), file=sys.stderr)
        print("I cannot choose: please use --host", file=sys.stderr)
        sys.exit(1)
    elif not hosts:
        print(F"No hosts found on db.debian.org for arch {arch}")
        sys.exit(1)
    return hosts[0]


class Porterbox:
    def __init__(self, host, username=None):
        self.host = host
        if "." not in host:
            # assume host is in debian.org domain
            self.hostname = host + ".debian.org"
        else:
            self.hostname = host

        self.config = paramiko.config.SSHConfig()
        ssh_config_pathname = os.path.expanduser("~/.ssh/config")
        if os.path.exists(ssh_config_pathname):
            log.debug("Load ssh config from %s", ssh_config_pathname)
            with open(ssh_config_pathname, "rt") as fd:
                self.config.parse(fd)

        self.host_config = self.config.lookup(self.hostname)
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        user_known_hosts = self.host_config.get("userknownhostsfile", None)
        if user_known_hosts is not None:
            log.debug("Load UserKnownHostsFile %s", ssh_config_pathname)
            self.client.load_host_keys(os.path.expanduser(user_known_hosts))
        try:
            self.client.connect(self.hostname, username=username)
        except paramiko.ssh_exception.PasswordRequiredException:
            self.client.connect(self.hostname, look_for_keys=False, username=username)

    def exec_command(self, cmd: str):
        log.debug("%s: running %s", self.hostname, cmd)
        chan = self.client.get_transport().open_session()
        chan.exec_command(cmd)
        chan.shutdown_write()
        b_stdout = bytearray()
        b_stderr = bytearray()
        status = None
        while True:
            if chan.recv_ready():
                b_stdout += chan.recv(4096)
            elif chan.recv_stderr_ready():
                b_stderr += chan.recv_stderr(4096)
            elif chan.exit_status_ready():
                status = chan.recv_exit_status()
                chan.close()
                break
            else:
                time.sleep(0.1)
        return b_stdout, b_stderr, status

    def run_checked(self, cmd: str):
        """
        Run a remote command returning its stdout, stderr and exit status
        """
        stdout, stderr, status = self.exec_command('sh -c ' + shlex.quote(cmd))
        if status:
            raise subprocess.CalledProcessError(status, F"{self.hostname}:{cmd}", output=stdout, stderr=stderr)
        return stdout

    def run_interactive(self, cmd: str, check: bool=False) -> int:
        """
        Run a remote command sending its stdout and stderr to stdout and
        stderr, and returning its exit status
        """
        log.info("%s: running %s", self.hostname, cmd)
        chan = self.client.get_transport().open_session()
        chan.setblocking(0)
        chan.exec_command(cmd)
        chan.shutdown_write()
        stdout_decoder = codecs.getincrementaldecoder("utf8")(errors="replace")
        stderr_decoder = codecs.getincrementaldecoder("utf8")(errors="replace")
        while True:
            if chan.recv_ready():
                sys.stdout.write(stdout_decoder.decode(chan.recv(4096)))
                sys.stdout.flush()
            elif chan.recv_stderr_ready():
                sys.stderr.write(stderr_decoder.decode(chan.recv_stderr(4096)))
                sys.stdout.flush()
            elif chan.exit_status_ready():
                status = chan.recv_exit_status()
                chan.close()
                break
            else:
                time.sleep(0.1)

        if check and status:
            raise subprocess.CalledProcessError(status, F"{self.hostname}:{cmd}")

        return status

    def get_chroots(self):
        stdout = self.run_checked("schroot -l")
        return stdout.decode("utf8").splitlines()

    def list_sessions(self):
        stdout = self.run_checked("schroot-list-sessions")
        return [line.split(":", 1)[0] for line in stdout.decode("utf8").splitlines()]

    def whoami(self):
        return self.run_checked("whoami").strip().decode("utf8")


class Chroot:
    def __init__(self, porterbox: Porterbox, dist: str, arch: str, package: str, reuse=False):
        self.porterbox = porterbox
        self.dist = dist
        self.arch = arch
        self.package = package
        self.reuse = reuse
        self.user = porterbox.whoami()
        self.session_id = F"{self.user}_{dist}_{arch}_{package}"

    @property
    def cmd_enter(self):
        return F"schroot -r -c {self.session_id}"

    @property
    def cmd_end(self):
        return F"schroot -e -c {self.session_id}"

    @property
    def workdir(self):
        return F"{self.package}_{self.dist}_{self.arch}"

    def setup(self):
        if self.session_id in self.porterbox.list_sessions():
            if self.reuse:
                log.info("Reusing existing session %s", self.session_id)
            else:
                raise Fail(F"Session {self.session_id} already exists on {self.porterbox.host}. You can terminate it with {self.cmd_end}")
        else:
            schroot_name = F"{self.dist}_{self.arch}-dchroot"
            if "chroot:" + schroot_name not in self.porterbox.get_chroots():
                raise Fail(F"Chroot {schroot_name} not found in {self.porterbox.host}")
            log.info("Creating chroot")
            self.porterbox.run_interactive(F"schroot -b -c {schroot_name} -n {self.session_id}", check=True)

        log.info("Updating chroot")
        self.porterbox.run_interactive(F"dd-schroot-cmd -c {self.session_id} apt-get update", check=True)
        self.porterbox.run_interactive(F"dd-schroot-cmd -y -c {self.session_id} apt-get upgrade", check=True)
        log.info("Installing build-deps")
        self.porterbox.run_interactive(F"dd-schroot-cmd -y -c {self.session_id} apt-get build-dep {self.package}", check=True)
        self.porterbox.run_checked(F"mkdir -p ~/{self.workdir}")

    def run_interactive(self, cmd: str, check: bool=False) -> int:
        """
        Run a remote command inside the chroot, sending its stdout and stderr
        to stdout and stderr, and returning its exit status
        """
        quoted_cmd = shlex.quote(cmd)
        return self.porterbox.run_interactive(F"schroot -r -c {self.session_id} -- sh -c {quoted_cmd}", check=check)

    def cleanup(self):
        self.porterbox.run_checked(F"rm -rf ~/{self.workdir}")
        self.porterbox.run_checked(self.cmd_end)


class Git:
    class Progress(git.remote.RemoteProgress):
        def update(self, op_code, cur_count, max_count=None, message=''):
            log.info("git:%s", self._cur_line)

    def __init__(self, chroot):
        self.chroot = chroot
        self.remote_workdir = F"{chroot.workdir}/{chroot.package}"
        self.remote_name = F"{chroot.porterbox.host}_{chroot.dist}_{chroot.arch}"
        self.remote_url = F"ssh://{chroot.porterbox.hostname}/home/{chroot.user}/{self.remote_workdir}"
        self.remote = None
        try:
            self.repo = git.Repo(".", search_parent_directories=True)
        except git.InvalidGitRepositoryError:
            raise Fail("--git requires running inside the git working directory")

    def lookup_existing_remote(self):
        """
        Set self.remote to an existing remote, or to None if it does not exist
        """
        for remote in self.repo.remotes:
            if remote.name == self.remote_name:
                # Check that the endpoint is right
                for url in remote.urls:
                    if url == self.remote_url:
                        self.remote = remote
                        break
                if self.remote is None:
                    raise Fail(F"local git repo already has a remote {self.remote_name} but it does not point to {self.remote_url}")
                break

    def create_remote(self):
        self.lookup_existing_remote()
        if self.remote is None:
            log.info("Creating remote %s", self.remote_name)
            self.chroot.porterbox.run_checked(F"mkdir -p ~/{self.remote_workdir}")
            self.chroot.porterbox.run_checked(F"bash -c 'cd ~/{self.remote_workdir} && test -d .git || git init && git config receive.denyCurrentBranch ignore'")
            self.remote = self.repo.create_remote(self.remote_name, self.remote_url)

    def push_to_remote(self):
        branch = self.repo.active_branch.name
        log.info("Pushing branch %s to %s", branch, self.remote_name)
        self.remote.push(refspec=F"{branch}:{branch}", progress=self.Progress())
        self.chroot.porterbox.run_checked(F"bash -c 'cd ~/{self.remote_workdir} && git reset --hard'")

    def cleanup(self):
        self.lookup_existing_remote()
        if self.remote is None:
            self.repo.delete_remote(self.remote)
            self.remote = None


def main():
    parser = argparse.ArgumentParser(
            description="set up a build environment to debug a package on a porterbox")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")
    parser.add_argument("--debug", action="store_true", help="debug output")
    parser.add_argument("--cleanup", action="store_true", help="cleanup a previous build, removing porterbox data and git remotes")
    parser.add_argument("--git", action="store_true", help="setup a git clone of the current branch")
    parser.add_argument("--reuse", action="store_true", help="reuse an existing session")
    parser.add_argument("--dist", action="store", default="sid", help="distribution (default: sid)")
    parser.add_argument("--host", action="store", help="hostname to use (autodetected by default)")
    parser.add_argument("--user", action="store", help="user login name on the Debian porterbox")
    parser.add_argument("arch", metavar="arch", help="architecture name")
    parser.add_argument("package", nargs="?", help="package name")

    args = parser.parse_args()

    log_format = "%(asctime)-15s %(levelname)s %(message)s"
    level = logging.WARN
    if args.debug:
        level = logging.DEBUG
    elif args.verbose:
        level = logging.INFO

    logging.basicConfig(level=level, stream=sys.stderr, format=log_format)

    if args.host:
        host = args.host
    else:
        host = find_host(args.arch)
    log.info("Using hostname %s", host)

    if args.user:
        username = args.user
    else:
        username = None

    package = args.package
    if package is None:
        # Auto detect package name from debian/control
        if not os.path.exists("debian/control"):
            raise Fail("Please specify a package name or run in a debian source directory")
        with open("debian/control", "rt") as fd:
            re_source = re.compile(r"^Source: (\S+)$")
            for line in fd:
                mo = re_source.match(line)
                if not mo:
                    continue
                package = mo.group(1)
                break
            log.info("Found package name %s from debian/control", package)

    porterbox = Porterbox(host, username=username)
    chroot = Chroot(porterbox, args.dist, args.arch, package, reuse=args.reuse)

    if args.cleanup:
        if args.git:
            repo = Git(chroot=chroot)
            repo.cleanup()
        chroot.cleanup()
    else:
        chroot.setup()

        if args.git:
            repo = Git(chroot=chroot)
            repo.create_remote()
            repo.push_to_remote()
        else:
            # Acquire package source
            chroot.run_interactive(F"bash -c 'cd {chroot.workdir} && apt-get source {package}'", check=True)

        # If dgit eventually gets installed on porterboxes, this is how to do a checkout with dgit
        # porterbox.run_interactive(F"bash -c 'cd {chroot.workdir} && dgit clone {package} {args.dist}'", check=True)

        # A lot more can happen here
        #  - if run from a git directory, a git repo can be created in the workdir and a remote can be set up
        #  - then the current branch can be cloned remotely, or a ref of the user's choice

        print(F"Your porterbox environment for {package} on {args.arch} is ready on {porterbox.hostname} on {chroot.workdir}")
        print("Quick reference:")
        if args.git:
            print(F"git remote: {repo.remote.name}")
            print(F"git work directory: ~/{repo.remote_workdir}")
        print(F"Run chroot: {chroot.cmd_enter}")
        print(F"End chroot: {chroot.cmd_end}")
        print(F"Cleanup: rm -r ~/{chroot.workdir}")


if __name__ == "__main__":
    try:
        main()
    except subprocess.CalledProcessError as e:
        print(e, file=sys.stderr)
        print("Command stderr:", file=sys.stderr)
        sys.stderr.write(e.stderr.decode("utf-8", errors="replace"))
    except Fail as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    except Exception:
        log.exception("uncaught exception")
